<?php
/**
 * @file Client object for communicating with the Docvert server
 *
 */
class DocvertClient {
  /**
   * URL (string) the url of the remote docvert server.
   */
  protected $url;

  /**
   * Documents (array) structured as
   * Array([pipeline] = Array([filename] = Array('source_file_path' => local file path, 'output' = Array(paths))))
   * Note that the 'output' is only present after the conversion has occured.
   */
  protected $documents = array();

  /**
   * Options array for configure docverts conversion options.
   */

  /**
   * Default location to unzip returned files. Overridden with platform-specific directory in __construct()
   */
  protected $extractDirectory = '/tmp';

  /**
   * Initiate a docvert client.
   * @param url (string) the url of the remote docvert server.
   */
  public function __construct($url, $integrityCheck = TRUE) {
    if (substr($url, strlen($url) -1) != '/') {
      $this->error('Docvert URLs should end in "/" . URL was: ' . $url);
    }
    $this->url = $url;
    $this->extractDirectory = sys_get_temp_dir();
    $this->integrityCheck = $integrityCheck;
  }


  /**
   * Docvert-specific errors
   *
   */
  private function error($message) {
    if (strlen($message) > 2000) { //we have to manually print it because PHP's Exception() because Exception() will truncate messages at 2kB
      print $message;
      $message = NULL;
    }
    throw new Exception($message);
  }

  /**
   * Add a single document to be converted.
   */
  public function addDocument($path, $pipeline = 'autopipeline:simple webpage', $autopipeline = 'Nothing (one long page)') {
    return $this->addDocuments(array($path), $pipeline, $autopipeline);
  }

  /**
   * Clear out stored documents
   */
  public function clearDocuments() {
    $this->documents = array();
  }

  /**
   * Add multiple documents to be converted.
   */
  public function addDocuments(Array $paths, $pipeline = 'autopipeline:simple webpage', $autopipeline = 'Nothing (one long page)') {
    //TODO: use autopipeline
    if (!isset($this->documents[$pipeline])) {
      $this->documents[$pipeline] = array();
    }
    foreach ($paths as $path) {
      if (file_exists($path)) {
        if ($this->integrityCheck) {
          if (array_key_exists(basename($path), $this->documents[$pipeline]) && sha1_file($this->documents[$pipeline][basename($path)]['source_file_path']) != sha1_file($path)) {
            print $this->documents[$pipeline][basename($path)]['source_file_path'];
            $this->error('Already have a file of name "' . basename($path) . '".');
          }
        }
        $this->documents[$pipeline][basename($path)] = array('source_file_path' => $path);
      }
    }
    return $this;
  }

  public function getPipelines() {
    $pipelinesUrl = $this->url . 'list-pipelines.php?format=json';
    $response = NULL;
    try {
      $session = curl_init($pipelinesUrl);
      curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
      $response = curl_exec($session);
      if ($response === FALSE) {
        $this->error(curl_error($session));
      }
      curl_close($session);
    }
    catch (Exception $exception) {
      $this->error(curl_error($session));
    }
    $pipelinesJson = json_decode($response);
    if ($pipelinesJson === NULL) {
      $this->error('Cannot decode JSON from ' . $pipelinesUrl);
    }
    $pipelines = array(
      'pipelines' => array(),
      'autopipelines' => array(),
    );
    foreach ($pipelinesJson->pipelines as $pipelineName => $pipelineType) {
      $pipelines['pipelines'][(string) $pipelineName] = (string) $pipelineType;
    }
    foreach ($pipelinesJson->autopipelines as $pipelineName => $pipelineType) {
      $pipelines['autopipelines'][(string) $pipelineName] = (string) $pipelineType;
    }
    return $pipelines;
  }

  private function formatPathsForCurl($paths) {
    $copy = array();
    foreach ($paths as $item) {
      $copy[$item['source_file_path']] = '@' . $item['source_file_path'];
    }
    return $copy;
  }

  private function getTemporaryDirectory($prefix = "docvert-") {
    $exitAfterXLoops = 10;
    do {
      $temporaryDirectory = $this->extractDirectory . DIRECTORY_SEPARATOR . $prefix . mt_rand(0, 9999999);
      $exitAfterXLoops--;
      if ($exitAfterXLoops <= 0) {
        $this->error("Unable to create temporary directory at " . $temporaryDirectory);
      }
    } while (@!mkdir($temporaryDirectory, 0700));
    return $temporaryDirectory;
  }

  /**
   * Send request to docvert server.
   *
   * @return (array) a list of extracted directories.
   */
  public function execute() {
    foreach ($this->documents as $pipeline => $documents) {
      $zipData = $this->callWebService($documents, $pipeline);
      $responseZipPath = tempnam($this->extractDirectory, 'docvert');
      $handle = fopen($responseZipPath, "w");
      fwrite($handle, $zipData);
      fseek($handle, 0);
      $zip = new ZipArchive;
      $resource = $zip->open($responseZipPath);
      if ($resource !== TRUE) {
        $this->error($zip->GetStatusString());
      }
      $outputDirectory = $this->getTemporaryDirectory();
      $zip->extractTo($outputDirectory);
      if ($this->integrityCheck) {
        $this->responseIntegrityCheck($outputDirectory, $zip, $pipeline);
      }
      return $this->documents;
    }
  }


  /**
   * Check that we got back documents for every conversion
   *
   * Will call error() if there's a problem, otherwise returns bool
   */
  public function responseIntegrityCheck($outputDirectory, $zip, $pipeline) {
    for ($i = 0; $i < $zip->numFiles; $i++) {
      $zipPath = $zip->getNameIndex($i);
      $outputPath = $outputDirectory . DIRECTORY_SEPARATOR . $zipPath;
      if (!file_exists($outputPath)) {
        $this->error("Expected file doesn't exist at " . $outputPath);
      }
      $directory = substr($zipPath, 0, strpos($zipPath, '/'));
      if (!array_key_exists($directory, $this->documents[$pipeline])) {
        $this->error('Document/Response mismatch: Received response to document that was not sent. File "' . $directory . '" not found in "' . implode('", "', array_keys($this->documents[$pipeline])) . '"');
      }
      if (!array_key_exists('output', $this->documents[$pipeline][$directory])) {
        $this->documents[$pipeline][$directory]['output'] = array();
      }
      $this->documents[$pipeline][$directory]['output'][] = $outputPath;
    }
    return TRUE;
  }

  /**
   * Calls the Docvert web service and returns ZIP data.
   *
   * @return (string) of ZIP data.
   */
  private function callWebService($documents, $pipeline, $autopipeline = 'Nothing (one long page)') {
    $response = NULL;
    try {
      $session = curl_init($this->url . 'web-service.php');
      curl_setopt($session, CURLOPT_POST, TRUE);
      curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
      $postFields = $this->formatPathsForCurl($documents);
      $postFields['pipeline'] = $pipeline;
      $postFields['converter'] = 'pyodconverter';
      $postFields['afterconversion'] = 'downloadZip';
      $postFields['autopipeline'] = $autopipeline;
      curl_setopt($session, CURLOPT_POSTFIELDS, $postFields);
      $response = curl_exec($session);
      if ($response === FALSE) {
        $this->error(curl_error($session));
      }
      if (!$this->is_a_zip($response)) {
        $this->error($response);
      }
      curl_close($session);
    }
    catch (Exception $exception) {
      $this->error(curl_error($session));
    }
    return $response;
  }

  /**
   * Checks whether arbitrary data looks like a ZIP
   *
   * @return (bool) whether it looks like a ZIP.
   */
  public static $zipMagicNumber = 'PK';
  public function is_a_zip($data) {
    return (substr($data, 0, strlen(DocvertClient::$zipMagicNumber)) == DocvertClient::$zipMagicNumber);
  }

}
