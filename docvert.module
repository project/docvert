<?php
/**
 * @file Integration with Docvert service - transforms MS Word documents into HTML and Drupal nodes
 */

include dirname(__FILE__) . '/docvert_widget.inc';

/**
 * Process any file attachments that use the docvert_widget
 */
function docvert_nodeapi(&$node, $op, $a3, $a4) {
  # TODO dman - this seems to be running twice. I think just 'presave' is all that is needed
  if ($op != 'update' && $op != 'presave') {
    return;
  }

  // Get current files and add to docvert
  $type_info = content_types($node->type);
  foreach ($type_info['fields'] as $field_name => $field_info) {
    if ($field_info['widget']['type'] != 'docvert_widget') {
      continue;
    }
    $uploaded_files = array();
    foreach ($node->$field_name as $idx => $upload) {
      // A file wasn't uploaded so skip to the next item
      if (empty($upload['fid'])) {
        continue;
      }
      $uploaded_files[$idx] = $upload['filepath'];
    }
    if (empty($uploaded_files)) {
      continue;
    }
    module_load_include('inc', 'docvert');
    $success = docvert_documents_to_string($uploaded_files, $node->body, $field_info['widget']['docvert_pipeline']);
  }
}

/**
 * Implementation of hook_menu
 */
function docvert_menu() {
  $items = array(
    'admin/settings/docvert' => array(
      'title' => 'Docvert',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('docvert_settings'),
      'access arguments' => array('access administration pages'),
      'description' => t('Configure the Docvert MSWord conversion tool.'),
  ),
    'admin/content/import-book' => array(
      'title' => 'Import Book',
      'page callback' => 'docvert_import_book_page',
      'access arguments' => array('import documents'),
      'description' => t('Upload a Word document as HTML pages organised in a book.'),
    ),
  );
  return $items;
}

function docvert_import_book_page() {
  if (!class_exists('XSLTProcessor')) {
    return t("PHP must be built with XSL support enabled for this feature to work. If this site is hosted on a debian environment install the package php5-xsl. For more details about installing XSL see !link", array('!link' => l('http://php.net/manual/en/book.xsl.php', 'php.net/manual/en/book.xsl.php', array('external' => TRUE))));
  }
  if (!module_exists('book')) {
    return t("The Book module must be enabled for this feature to operate.");
  }
  $output = drupal_get_form('docvert_import_book_form');
  return $output;
}

function docvert_import_book_form() {
  $form = array();
  $form['#attributes'] = array(
    'enctype' => 'multipart/form-data',
  );
  $form['document'] = array(
    '#type' => 'file',
    '#title' => 'Document to convert',
    '#description' => 'Select a document to upload and convert to Drupal book, note that due to the requirments of the Book module all child pages will be immediately set as published',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Upload',
  );
  return $form;
}

//This will just be a stub function around the actual processor eventually
function docvert_import_book_form_submit($form, $form_values) {
  global $user;

  $document = file_save_upload('document');

  module_load_include('inc', 'docvert');
  module_load_include('inc', 'node', 'node.pages');

  $docvert = docvert_client();
  $docvert->addDocument($document->filepath, 'regularpipeline:docbook');
  if (!$pipelines = $docvert->execute()) {
    watchdog('Docvert', "An error occured with Docvert. Document conversion failed.", array(), WATCHDOG_ERROR);
    drupal_set_message("Document file could not be converted into a Docbook.", 'error');
    return FALSE;
  }

  foreach ($pipelines as $pipeline => $files) {
    foreach ($files as $filename => $info) {
      // first item from $info['output'] is the directory.
      // We'll use this to find the index.
      $directory = $info['output'][0];
      $index = $directory . 'index.html';
      if (!file_exists($index)) {
        watchdog('docvert', 'Could not find document contents', array(), WATCHDOG_ERROR);
        return FALSE;
      }

      $xml_string = file_get_contents($index);
      $xml_string = docvert_handle_document_images($info['output'], $xml_string);
      $xml = simplexml_load_string($xml_string);

      $title_elements = $xml->xpath('db:title');
      if ($title_elements !== FALSE) {
        $title = $title_elements[0][0];
      }

      $preface_elements = $xml->xpath('db:preface');
      if ($preface_elements !== FALSE) {
        $preface = $preface_elements[0]->asXML();
      }

      $chapter_elements = $xml->xpath('db:chapter');
      $chapter_title_elements = $xml->xpath('db:chapter/db:title');

      $filename = $document->filename;

      $preface_node = (object) array(
        'type' => 'book',
        'title' => "$filename :: Preface",
        'uid' => $user->uid,
        'status' => 1,
        'book' => array(
          'nid' => 'new',
          'bid' => 'new',
          'original_bid' => 0,
          'router_path' => 'node/%',
          'module' => 'book',
          'weight' => 0,
          'options' => array(),
        ),
        'body' => '',
        'format' => 2, //full html
      );
      node_save($preface_node);
      drupal_set_message(t('Created preface/book: !link', array('!link' => l($preface_node->title, 'node/' . $preface_node->nid))));

      $nodes = array();
      for ($i = 0; $i < count($chapter_elements); $i++) {
        $new_node = (object) array(
          'type' => 'book',
          'title' => !empty($chapter_title_elements[$i][0]) ? $chapter_title_elements[$i][0] : "$filename :: Chapter " . ($i + 1),
          'uid' => $user->uid,
          'status' => 1,
          'book' => array(
            'bid' => $preface_node->nid,
            'router_path' => 'node/%',
            'module' => 'book',
            'weight' => ($i + 1),
          ),
          'body' => '',
          'format' => 2,
        );
        node_save($new_node);
        drupal_set_message(t("Created new chapter: (!count) !link ", array('!count' => $i + 1, '!link' => l($new_node->title, 'node/' . $new_node->nid))));
        $nodes[] = array(
          'node' => $new_node,
          'xml' => $chapter_elements[$i]->asXML(),
          'body' => '',
        );
      }

      //Do xml to html conversion now
      //Setup the xslt conversion
      $xsl = new XSLTProcessor();
      $xsldoc = new DOMDocument();
      $xsldoc->load(drupal_get_path('module', 'docvert') . '/docbook-html.xslt');
      $xsl->importStylesheet($xsldoc);

      //Convert and save the body for the preface
      $dom = new DOMDocument();
      //Have to wrap content in db book so that xslt works correctly
      $dom->loadXML('<db:book xmlns:db="http://docbook.org/ns/docbook" xmlns:html="http://www.w3.org/1999/xhtml">' . $preface . '</db:book>');
      $preface_node->body = $xsl->transformToXML($dom);
      node_save($preface_node);

      //Convert and save the body for the child pages
      //drupal_set_message('Got '.count($nodes).' nodes');
      //drupal_set_message('<pre>'.print_r($nodes, true).'</pre>');
      foreach ($nodes as &$item) {
        $dom->loadXML('<db:book xmlns:db="http://docbook.org/ns/docbook" xmlns:html="http://www.w3.org/1999/xhtml">' . $item['xml'] . '</db:book>');
        $item['body'] = $xsl->transformToXML($dom);
        $item['node']->body = $item['body'];
        node_save($item['node']);
      }

    } //foreach files in pipleine
  } //foreach pipelines
}

/**
 * Implementation of hook_perm.
 */
function docvert_perm() {
  return array('Use docvert', 'import documents');
}

/**
 * Implementation of hook_elements().
 */
function docvert_elements() {
  $filefield_elements = module_invoke('filefield', 'elements');
  $elements = array();
  $elements['docvert_widget'] = $filefield_elements['filefield_widget'];
  $elements['docvert_widget']['#process'][] = 'docvert_widget_process';
  return $elements;
}

/**
 * Implementation of hook_cron
 */
function docvert_cron() {
  global $user;
  module_load_include('inc', 'node', 'node.pages');
  module_load_include('inc', 'docvert');
  module_load_include('module', 'filefield');
  module_load_include('inc', 'filefield', 'filefield_field');
  foreach (node_get_types('names') as $type => $name) {
    // Check if we're monitoring a directory got documents.
    if (!$directory = variable_get('docvert_type_' . $type, FALSE)) {
      continue;
    }
    if (!is_dir($directory)) {
      watchdog('Docvert', "No such directory '!directory", array('!directory' => $directory), WATCHDOG_WARNING);
      continue;
    }
    $docvert_fields = array();
    $content_type = content_types($type);
    foreach ($content_type['fields'] as $field => $info) {
      if ($info['widget']['module'] == 'docvert') {
        $docvert_fields[$field] = $info['widget'];
      }
    }
    if (empty($docvert_fields)) {
      continue;
    }
    // Check there are files to send to Docvert.
    if (!$handle = opendir($directory)) {
      watchdog('Docvert', "Cannot open directory '!directory", array('!directory' => $directory), WATCHDOG_WARNING);
      continue;
    }
    while (FALSE !== ($file = readdir($handle))) {
      if (!is_file("$directory/$file")) {
        continue;
      }
      $node_fields = array();
      foreach ($docvert_fields as $field => $info) {
        $exts = explode(' ', $info['file_extensions']);
        if (in_array(pathinfo($file, PATHINFO_EXTENSION), $exts)) {
          $body = '';
          docvert_documents_to_string(array("$directory/$file"), $body, $info['docvert_pipeline']);
          $uploaded_file = docvert_save_upload("$directory/$file", file_directory_path());
          $uploaded_file->list = 1;
          $uploaded_file->uid = $user->uid;
          $node_fields[] = (array) $uploaded_file;
        }
      }
      if (!empty($body)) {
        $form_state = array();
        $form_state['values']['uid'] = $user->uid;
        $form_state['values']['type'] = $type;
        $form_state['values']['title'] = $file;
        $form_state['values']['body'] = $body;
        $form_state['values'][$field] = $node_fields;
        $form_state['values']['format'] = 2; //Full html

        $node = (object) $form_state['values'];
        $form_state['values']['op'] = "Save";

        //Creates the new node and attaches the files
        drupal_execute($type . "_node_form", $form_state, $node);
      }
      //Delete file
      if (!@unlink("$directory/$file")) {
        watchdog('Docvert', "Unable to remove $directory/$file. File will be sent to docvert next cron run.", array(), WATCHDOG_ERROR);
      }
    }
    closedir($handle);
  }
}

function docvert_save_upload($filepath, $dest, $replace = FILE_EXISTS_RENAME) {
  //This function is based heavily off file_save_upload in drupal core

  $extensions .= ' ' . variable_get("upload_extensions_$rid",
      variable_get('upload_extensions_default', 'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp'));
  $file = new stdClass();
  $file->filename = file_munge_filename(trim(basename($filepath), '.'), $extensions);
  $file->filepath = $filepath;
  $file->filemime = file_get_mimetype($file->filename);
  $file->data     = array();

  $file->source = $source;
  $file->destination = file_destination(file_create_path($dest . '/' . $file->filename), $replace);
  $file->filesize = filesize($filepath);

  // Rename potentially executable files, to help prevent exploits.
  if (preg_match('/\.(php|pl|py|cgi|asp|js)$/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
    $file->filemime = 'text/plain';
    $file->filepath .= '.txt';
    $file->filename .= '.txt';
    // As the file may be named example.php.txt, we need to munge again to
    // convert to example.php_.txt, then create the correct destination.
    $file->filename = file_munge_filename($file->filename, $extensions);
    $file->destination = file_destination(file_create_path($dest . '/' . $file->filename), $replace);
  }

  $file->filepath = $file->destination;
  //Copy file now
  if (!copy($filepath, $file->filepath)) {
    form_set_error($source, t('File upload error. Could not move uploaded file.'));
    watchdog('docvert', 'Upload error. Could not move uploaded file %file to destination %destination.', array('%file' => $file->filename, '%destination' => $file->filepath));
    return 0;
  }

  // If we made it this far it's safe to record this file in the database.
  $file->uid = $user->uid;
  $file->status = FILE_STATUS_TEMPORARY; //Should be set to permanent after finished saving
  $file->timestamp = time();
  drupal_write_record('files', $file);

  // Add file to the cache.
  $upload_cache[$source] = $file;
  return $file;
}

/**
 * Admin page for docvert.
 */
function docvert_settings() {
  $form = array(
    'docvert_content_types' => array(
      '#type' => 'fieldset',
      '#title' => 'Content Directories',
    ),
  );
  foreach (node_get_types('names') as $type => $name) {
    $info = content_types($type);
    foreach ($info['fields'] as $field => $field_info) {
      if ($field_info['widget']['module'] == 'docvert') {
        $form['docvert_content_types']['docvert_type_' . $type] = array(
          '#title' => t('!name docs', array('!name' => $name)),
          '#type' => 'textfield',
          '#default_value' => variable_get('docvert_type_' . $type, ''),
          '#description' => t('Write an absolute location where Drupal should look for documents to convert into !name content types.', array('!name' => $type)),
        );
      }
    }
  }
  $form['docvert_url'] = array(
    '#title' => 'Docvert Server',
    '#type' => 'textfield',
    '#description' => 'The URL of the docvert service. Should end in a /',
    '#default_value' => variable_get('docvert_url', ''),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 *
 * Ensure the Docvert server is available and correct.
 */
function docvert_settings_validate($form, &$form_state) {
  // Ensure a trailing slash on the given URL
  if ($form_state['values']['docvert_url'] == rtrim($form_state['values']['docvert_url'], '/')) {
    form_set_value($form['docvert_url'], $form_state['values']['docvert_url'] . '/', $form_state);
  }
  // Make a test request on the docvert server
  $docvert_url = l($form_state['values']['docvert_url'], $form_state['values']['docvert_url']);
  module_load_include('inc', 'docvert', 'docvert.client');

  $dv_client = new DocvertClient($form_state['values']['docvert_url'], '');
  try {
    $pipelines = $dv_client->getPipelines();
  }
  catch (Exception $e) {
    form_set_error('docvert_url', t('Invalid Response from Docvert URL !docvert_url. %message', array('!docvert_url' => $docvert_url, '%message' => $e->getMessage())));
  }

  if (empty($pipelines)) {
    form_set_error('docvert_url', t('Invalid Docvert URL. I wasn\'t able to retrieve an expected response from !docvert_url. Check that that is the location of the docvert directory on the correct target server.', array('!docvert_url' => $docvert_url)));
  }
  else {
    drupal_set_message("Looks like the Docvert server URL is responding OK");
  }
}

/**
 * Implementation of CCK's hook_widget_info().
 */
function docvert_widget_info() {
  return array(
    'docvert_widget' => array(
      'label' => t('Docvert'),
      'field types' => array('filefield'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array('default value' => CONTENT_CALLBACK_CUSTOM),
      'description' => t('An edit widget for document files'),
    ),
  );
}

/**
 * Implementation of hook_theme().
 */
function docvert_theme() {
  $filefield_themes = module_invoke('filefield', 'theme');
  $themes = array();
  foreach ((array) $filefield_themes as $theme => $info) {
    $themes[str_replace('filefield', 'docvert', $theme)] = array(
      'arguments' => $info['arguments'],
      'function' => 'theme_' . $theme,
    );
  }
  return $themes;
}

/**
 * Implementation of CCK's hook_widget_settings().
 */
function docvert_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      return docvert_widget_settings_form($widget);
    case 'validate':
      return docvert_widget_settings_validate($widget);
    case 'save':
      return docvert_widget_settings_save($widget);
  }
}

/**
 * Implementation of CCK's hook_widget().
 *
 * Delegate to FileField.
 */
function docvert_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  // Start with the FileField widget as a basic start.
  // Note that FileField needs to modify $form by reference.
  $element = filefield_widget($form, $form_state, $field, $items, $delta);

  // Add docvert specific validators.
  $element['#upload_validators'] = array_merge($element['#upload_validators'], docvert_widget_upload_validators($field));
  return $element;
}

/**
 * Get the additional upload validators for a docvert field.
 *
 * @param $field
 *   The CCK field array.
 * @return
 *   An array suitable for passing to file_save_upload() or the file field
 *   element's '#upload_validators' property.
 */
function docvert_widget_upload_validators($field) {
  module_load_include('inc', 'docvert');
  $validators = array();

  // Match the default value if no file extensions have been saved at all.
  if (!isset($field['widget']['file_extensions'])) {
    $field['widget']['file_extensions'] = implode(' ', docvert_extensions());
  }

  // Ensure that only web images are supported.
  $extensions = array_filter(explode(' ', $field['widget']['file_extensions']));
  $validators['filefield_validate_extensions'][0] = implode(' ', array_intersect($extensions, docvert_extensions()));

  // Add the document validator as a basic safety check.
  //$validators['docvert_validate_is_document'] = array();

  return $validators;
}

/**
 * Implementation of CCK's hook_default_value().
 */
function docvert_default_value(&$form, &$form_state, $field, $delta) {
  return filefield_default_value($form, $form_state, $field, $delta);
}

/**
 * @defgroup "Theme Callbacks"
 * @{
 * @see docvert_theme().
 */
function theme_docvert($file) {
  return '';
}

/**
 * @} End defgroup "Theme Callbacks".
 */
