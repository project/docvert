<?php
include("docvert.client.inc");
$docvert_client = new DocvertClient('http://localhost/docvert/');
$pipelines = $docvert_client->getPipelines();
$resulting_files = false;
if($_FILES) {
    if(!is_valid_pipeline($pipelines['pipelines'], $_POST['pipeline']) || !is_valid_autopipeline($pipelines['autopipelines'], $_POST['autopipeline'])) {
        die("Not a valid pipeline. Was given ".htmlentities($_POST['pipeline']).' and '.htmlentities($_POST['autopipeline']));
    }
    foreach($_FILES as $file) {
        $temporaryDirectory = getTemporaryDirectory();
        $readable_path = $temporaryDirectory.DIRECTORY_SEPARATOR.$file['name'];
        move_uploaded_file($file['tmp_name'], $readable_path);
        $docvert_client->addDocument($readable_path, $_POST['pipeline'], $_POST['autopipeline']);
        }
    $resulting_files = $docvert_client->execute();
}

function is_valid_pipeline($pipelines, $given_pipeline) {
    $given_pipeline = explode(':', $given_pipeline);
    foreach($pipelines as $pipeline_name => $pipeline_type) {
        if($pipeline_type == $given_pipeline[0] && $pipeline_name == $given_pipeline[1]) return true;
    }
    return false;
}

function is_valid_autopipeline($autopipelines, $given_autopipeline) {
    foreach($autopipelines as $autopipeline_name) {
        if($autopipeline_name == $given_autopipeline) return true;
    }
    return false;
}

function getTemporaryDirectory($prefix = "docvert-") {
    $exitAfterXLoops = 10;
    do {
	    $temporaryDirectory = sys_get_temp_dir().DIRECTORY_SEPARATOR.$prefix.mt_rand(0, 9999999);
	    $exitAfterXLoops--;
	    if($exitAfterXLoops <= 0) {
            $this->error("Unable to create temporary directory at ".$temporaryDirectory);
	    }
	} while(@!mkdir($temporaryDirectory, 0700));
    return $temporaryDirectory;
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Docvert test</title>
    <script type="text/javascript">
        var indexCounter = 0;
        function addFile() {
            indexCounter++
            var files_div = document.getElementById("files")
            var input_tag = document.createElement("input")
	        input_tag.type = "file"
	        input_tag.name = "random" + indexCounter
	        files_div.appendChild(input_tag)
        }
    </script>
    <style type="text/css">
        #files input {
            clear:both;
            width:100%;
        }
        .field {
            background:#eeeeee;
            margin:0px 0px 0.5em 0px;
            padding:0.5em;
        }
        #conversion {
            background: #ffffcc;
        }
        #conversion pre {
           width:100%;
           height:150px;
           overflow:auto;
        }
    </style>
</head>
<body>
<?php
if($resulting_files) {
    print '<div id="conversion">';
    print '<h1>Success!</h1>';
    print '<pre>';
    print_r($resulting_files);
    print '</pre>';
    print '</div>';
    }
?>
<form method="post" enctype="multipart/form-data">
    <h1>Docvert test</h1>
    <div id="files" class="field">
        (<a href="#add" onclick="addFile()">add</a>)
        <input type="file" name="random0">
    </div>
    <div id="pipelines" class="field">
        Pipeline <select name="pipeline">
            <?php
            foreach($pipelines["pipelines"] as $pipeline_name => $pipeline_type) {
                print '<option value="'.htmlentities($pipeline_type).':'.htmlentities($pipeline_name).'">'.htmlentities($pipeline_name);
                if($pipeline_type == "autopipeline") print ' *';
                print '</option>';
            }
            ?>
        </select>
    </div>
    <div id="autopipelines" class="field">
        Auto Pipeline: <select name="autopipeline">
            <?php
            foreach($pipelines["autopipelines"] as $autopipeline) {
                print '<option value="'.htmlentities($autopipeline).'">'.htmlentities($autopipeline).'</option>';
            }
            ?>
        </select>
    </div>

    <hr />
    <input type="submit" value="Send to Docvert"/>
</form>
</body>
</html>
