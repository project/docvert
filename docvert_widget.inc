<?php

/**
 * @file
 * docvert widget hooks and callbacks.
 */

/**
 * Implementation of CCK's hook_widget_settings($op = 'form').
 */
function docvert_widget_settings_form($widget) {
  module_load_include('inc', 'docvert');
  $form = module_invoke('filefield', 'widget_settings', 'form', $widget);

  if (!$widget['file_extensions']) {
    $form['file_extensions']['#default_value'] = implode(' ', docvert_extensions());
  }

  $form['docvert_pipeline'] = array(
    '#type' => 'select',
    '#title' => 'Docvert Pipeline',
    '#options' => docvert_pipelines(),
    '#default_value' => isset($widget['docvert_pipeline']) ? $widget['docvert_pipeline'] : 'autopipeline:simple webpage',
    '#description' => t('<a href="http://holloway.co.nz/docvert/faq.html#8">Pipelines</a> are provided on the Docvert server side. Install them over there to make them available here.'),
  );
  return $form;
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'validate').
 */
function docvert_widget_settings_validate($widget) {
  // Check that only web documents are specified in the callback.
  $extensions = array_filter(explode(' ', $widget['file_extensions']));
  if (count(array_diff($extensions, docvert_extensions()))) {
    form_set_error('file_extensions', t('Only documents extensions (!extensions) are supported through the document widget. If needing to upload other types of documents, change the widget to use a standard file upload.', array('!extensions' => implode(' ', docvert_extensions()))));
  }
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'save').
 */
function docvert_widget_settings_save($widget) {
  $filefield_settings = module_invoke('filefield', 'widget_settings', 'save', $widget);
  return array_merge($filefield_settings, array('docvert_pipeline'));
}

/**
 * Element #process callback function.
 */
function docvert_widget_process($element, $edit, &$form_state, $form) {
  // Bascially, push through to docvert, grab html and put in node body.
  return $element;
}
