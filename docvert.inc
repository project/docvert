<?php
/**
 * @file Utility functions for docvert module.
 * Mostly to provide a functional interface to the OO Docvert Client library
 */

/**
 * Retrieve a docvert client singleton.
 */
function docvert_client() {
  static $singleton;
  if (!$singleton) {
    module_load_include('inc', 'docvert', 'docvert.client');
    $singleton = new DocvertClient(variable_get('docvert_url', ''));
  }
  return $singleton;
}

/**
 * Get the frontpage from docvert server once and convert it to SimpleXML.
 */
function docvert_frontpage() {
  static $xml;
  if (!$xml) {
    $sample_url = variable_get('docvert_url', '') . '/sample-use.php';
    watchdog('docvert', 'Making request to docvert server for available conversion pipeline info.', array('!sample_url' => l($sample_url, $sample_url)), WATCHDOG_INFO);
    $result = drupal_http_request($sample_url);
    $dom = new DOMDocument;
    $dom->loadHTML($result->data);
    if (!$xml = simplexml_import_dom($dom)) {
      throw new Exception(t("Failed to load Docvert configurations from !url", array('!url' => variable_get('docvert_url', ''))));
    }
  }
  return $xml;
}

/**
 * Get pipeline options from the docvert server.
 */
function docvert_pipelines() {
  $pipes = array();
  try {
    foreach (docvert_frontpage()->xpath("//select[@id='pipeline']/option") as $pipe) {
      $pipes[(string) $pipe['value']] = (string) $pipe;
    }
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
  return $pipes;
}

/**
 * Get pipeline options from the docvert server.
 * TODO - this seems like a better option than screen-scraping, but need to check if it works on Docvert5 also
 */
function docvert_pipelines_2() {
  // Use the API call provided by the server.
  $docvert_client = docvert_client();
  try {
    $pipelines = $docvert_client->getPipelines();
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
  $pipes = array();
  foreach ($pipelines['pipelines'] as $name => $type) {
    $pipes["$type:$name"] = $name;
  }
  return $pipes;
}

/**
 * Convert documents into an HTML string.
 *
 * If there are more than one document, they each get concatenated into the one HTML string.
 * If the content has already been processed before, the re-processed document is re-inserted into the same place in the document using markers.
 * Content inserted by previous documents is deleted if the file attachment is deleted.
 *
 * @param array $documents A list of file paths
 * @param string $html Previous body text, updated by reference
 * @param $pipeline
 * @return bool Success
 */
function docvert_documents_to_string($documents, &$html, $pipeline = NULL) {
  watchdog('docvert', 'Sending documents %documents to Docvert server for conversion', array('%documents' => '[' . join(', ', $documents) . ']'), WATCHDOG_INFO);

  $docvert = docvert_client();
  $docvert->clearDocuments();
  $docvert->addDocuments($documents, $pipeline);
  try {
    $pipelines = $docvert->execute();
  }
  catch (Exception $e) {
    $strings = array('!message' => $e->getMessage());
    watchdog('Docvert', "An error occured with Docvert. Document conversion failed. !message", $strings, WATCHDOG_ERROR);
    drupal_set_message(t("Document files could not be converted into html. !message", $strings), 'error');
    return FALSE;
  }
  watchdog('docvert', 'Received results from Docvert server, now processing text', array(), WATCHDOG_INFO);
  foreach ($pipelines as $pipeline => $files) {
    foreach ($files as $filename => $info) {
      $idx = array_search($info['source_file_path'], $documents);
      // first item from $info['output'] is the directory.
      // We'll use this to find the index.
      $docvert_html = docvert_prepare_html($info['output']);
      $docvert_html = docvert_handle_document_images($info['output'], $docvert_html);
      $token = 'fid:' . $idx;
      $header = "<!-- docvert:$token -->";
      $footer = "<!-- /docvert:$token -->";
      if (strpos($html, $header) !== FALSE) {
        $regex = '/' . preg_quote($header, '/') . '(.*)' . preg_quote($footer, '/') . '/ms';
        $html = preg_replace($regex, $header . $docvert_html . $footer, $html);
      }
      else {
        $html .= $header . $docvert_html . $footer;
      }
    }
  }
  $token = '-- docvert\:fid\:(\d+) --';
  if (preg_match_all("/$token/m", $html, $matches)) {
    $deleted = array_diff($matches[1], array_keys($documents));
    if (!empty($deleted)) {
      foreach ($deleted as $fid) {
        $token = 'fid:' . $fid;
        $header = "<!-- docvert:$token -->";
        $footer = "<!-- /docvert:$token -->";
        $regex = '/' . preg_quote($header, '/') . '(.*)' . preg_quote($footer, '/') . '/ms';
        $html = preg_replace($regex, '', $html);
      }
    }
  }
  return TRUE;
}

function docvert_handle_document_images($documents, $html) {
  if (empty($documents)) {
    return;
  }
  $image_types = array('jpg', 'gif', 'png');
  $search_strings = array();
  $replacement_strings = array();
  foreach ($documents as $original_filename) {
    if (!is_file($original_filename)) {
      continue;
    }
    $file_ext = substr($original_filename, strrpos($original_filename, '.') + 1);
    if (!in_array($file_ext, $image_types)) {
      continue;
    }
    if (basename($original_filename) == 'docvert-thumbnail.png') {
      //Skip the generated thumbnail
      continue;
    }
    $new_filename = $original_filename;
    if (file_copy($new_filename)) {
      $search_strings[] = '"' . basename($original_filename) . '"';
      $replacement_strings[] = '"/' . $new_filename . '"';
      drupal_set_message("New image $new_filename has been uploaded");
    }
  }
  if (!empty($search_strings)) {
    return str_replace($search_strings, $replacement_strings, $html);
  }
  else {
    return $html;
  }
}

/**
 * Simple utility function to check if a file is an image.
 */
function docvert_file_is_document($file) {
  $file = (object) $file;
  return in_array($file->filemime, docvert_mimetypes());
}

function docvert_extensions() {
  return array('doc', 'docx', 'xls', 'xlsx', 'odt', 'pdf', 'rtf');
}

function docvert_mimetypes() {
  return array(
    'application/pdf',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.ms-excel',
    #    'application/vnd.openxmlformats-officedocument.presentationml.presentation' #    'application/vnd.ms-powerpoint',
    'application/vnd.oasis.opendocument.text',
  );
}

/**
 * Prepare an html documents from returned contents.
 */
function docvert_prepare_html($filelist) {
  // Assumption that all contents are in index.html.
  $directory = $filelist[0];
  $index = $directory . 'index.html';
  if (!file_exists($index)) {
    watchdog('docvert', 'Could not find document contents', array(), WATCHDOG_ERROR);
    return FALSE;
  }
  $xml = simplexml_load_file($index);
  return strtr($xml->body->asXML(), array('<body>' => '', '</body>' => ''));
}
