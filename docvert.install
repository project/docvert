<?php
/**
 * @file Install routines for docvert
 * 
 */
function docvert_enable() {
  if (module_exists('filefield')) {
    drupal_set_message(st('A new file attachment widget is now available for handling attachments to content types.'), 'info');
  }
  if (module_exists('book')) {
    drupal_set_message(st('You can now upload Word documents as "books" at !link.', array('!link', l('admin/content/import-book', 'admin/content/import-book'))), 'info');
  }
}

function docvert_requirements($phase) {
  $requirements = array();

  // Don't bother about phases
  if (!class_exists('XSLTProcessor')) {
    $requirements['docvert_xslt'] = array(
      'title' => st('Docvert requirements'),
      'value' => st('XSLT Processor unavailable'),
      'severity' => REQUIREMENT_ERROR,
      'description' => st(
        "PHP must be built with XSL support enabled for the docvert feature to work. If this site is hosted on a debian environment install the package php5-xsl. For more details about installing XSL see !link", 
        array('!link' => l('http://php.net/manual/en/book.xsl.php', 'php.net/manual/en/book.xsl.php', array('external' => TRUE)))
      ),
    );
  }
  if (!class_exists('ZipArchive')) {
    $requirements['docvert_ziparchive'] = array(
      'title' => st('Docvert requirements'),
      'value' => st('ZipArchive unpacker unavailable'),
      'severity' => REQUIREMENT_ERROR,
      'description' => st('The Docvert tool require EITHER book module - which allows a one-step process to turn a word document into a book OR CCK and filefield - which allows you to attach a Word Document to a node and populate the node content from that document (a new widget becomes available for handling attachments). Neither of these are strictly required, but you have to choose at least one for this tool to become available.'),
    );
  }
  if (!module_exists('book') && !module_exists('filefield')) {
    $requirements['docvert_modules'] = array(
      'title' => st('Docvert requirements'),
      'value' => st('Niether filefield.module or book.module are enabled'),
      'severity' => REQUIREMENT_WARNING,
      'description' => st(
        "PHP must be built with zip support enabled for the docvert feature to work. You might just be able to uncomment the line 'extension=zip.so' in your php.ini. Otherwise, see INSTALL.txt. See !link", 
        array('!link' => l('http://www.php.net/manual/en/zip.installation.php', 'http://www.php.net/manual/en/zip.installation.php', array('external' => TRUE)))
      ),
    );
  }
  return $requirements;
}